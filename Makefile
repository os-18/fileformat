# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

PROJECT=fileformat
BIN=build/${PROJECT}
DESTDIR=
PREFIX=usr/local

SRC=$(shell find source -name "*.d")

DC=ldc2
PHOBOS_LINKING=dynamic
AMALTHEA_LINKING=dynamic

PHOBOS_OPTS=   $(shell ./mh.sh $(DC) phobos_options $(PHOBOS_LINKING))
AMALTHEA_OPTS= $(shell ./mh.sh $(DC) amalthea_options $(AMALTHEA_LINKING))
ADDIT_OPTS=    $(shell ./mh.sh $(DC) addit_options)
RELEASE_OPTS=  $(shell ./mh.sh $(DC) release_options)
DEBUG_OPTS=    $(shell ./mh.sh $(DC) debug_options)
LINKING_OPTS=  $(PHOBOS_OPTS) $(AMALTHEA_OPTS) $(ADDIT_OPTS)

DOC_DIR=$(shell ./mh.sh $(DC) doc_dir)/$(PROJECT)

OUT=-of
ifeq ($(DC),gdc)
	OUT=-o
endif

.PHONY: release debug install uninstall clean dub

release: $(SRC)
	mkdir -p build/
	$(DC) $^ $(OUT)$(BIN) $(RELEASE_OPTS) -Jsource $(LINKING_OPTS)

debug: $(SRC)
	mkdir -p build/
	$(DC) $^ $(OUT)$(BIN) $(DEBUG_OPTS) -Jsource $(LINKING_OPTS)

INST_BINDIR=$(DESTDIR)/$(PREFIX)/bin/
INST_BASHCOMPDIR=$(DESTDIR)/$(PREFIX)/share/bash-completion/completions/
INST_DOCDIR=$(DESTDIR)/$(PREFIX)/$(DOC_DIR)/
INST_HELPDIR=$(DESTDIR)/$(PREFIX)/share/help
INST_MANDIR_EN=$(DESTDIR)/$(PREFIX)/share/man/man1
INST_MANDIR_RU=$(DESTDIR)/$(PREFIX)/share/man/ru/man1
INST_MANDIR_EO=$(DESTDIR)/$(PREFIX)/share/man/eo/man1

install:
	mkdir -p "$(INST_BINDIR)" "$(INST_BASHCOMPDIR)"
	mkdir -p "$(INST_DOCDIR)" "$(INST_HELPDIR)"
	mkdir -p "$(INST_MANDIR_EN)" "$(INST_MANDIR_RU)" "$(INST_MANDIR_EO)"
	install "$(BIN)" "${INST_BINDIR}"
	cp source/_$(PROJECT) "$(INST_BASHCOMPDIR)/$(PROJECT)" || true
	cp copyright $(INST_DOCDIR)
	cp -r share/help/* $(INST_HELPDIR)/
	gzip -9 -k -n share/man/$(PROJECT).1
	gzip -9 -k -n share/man/$(PROJECT).ru.1
	gzip -9 -k -n share/man/$(PROJECT).eo.1
	install share/man/$(PROJECT).1.gz    "$(INST_MANDIR_EN)/$(PROJECT).1.gz"
	install share/man/$(PROJECT).ru.1.gz "$(INST_MANDIR_RU)/$(PROJECT).1.gz"
	install share/man/$(PROJECT).eo.1.gz "$(INST_MANDIR_EO)/$(PROJECT).1.gz"
	rm share/man/*.1.gz

INST_HELPDIR_EN=$(INST_HELPDIR)/en_US/$(PROJECT)/
INST_HELPDIR_RU=$(INST_HELPDIR)/ru_RU/$(PROJECT)/
INST_HELPDIR_EO=$(INST_HELPDIR)/eo/$(PROJECT)/

uninstall:
	rm -f $(INST_BINDIR)/$(PROJECT)
	rm -f $(INST_BASHCOMPDIR)/$(PROJECT)
	rm -rf $(INST_DOCDIR)
	rm -rf $(INST_HELPDIR_EN) $(INST_HELPDIR_RU) $(INST_HELPDIR_EO)
	rm -f $(INST_MANDIR_EO)/$(PROJECT).1.gz
	rm -f $(INST_MANDIR_EN)/$(PROJECT).1.gz
	rm -f $(INST_MANDIR_RU)/$(PROJECT).1.gz

clean:
	rm -rf build/ .dub/ dub.selections.json

dub:
	dub build --compiler=$(DC) --force


ALL=.gitignore changelog copyright dub.sdl mh.sh LICENSE Makefile README.md \
summary copying/ man/ share/ source/

VERSION=$(shell cat source/version)
PKGNAME=$(PROJECT)-$(VERSION)

TARNAME=$(PKGNAME).tar.xz

tarball:
	mkdir -p build/$(PKGNAME)
	cp -p -r $(ALL) build/$(PKGNAME)/
	cd build ; tar --xz -cf $(TARNAME) $(PKGNAME)
