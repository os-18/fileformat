fileformat — инструмент для определения формата файла.

Использование:

fileformat <файл> ... [-m|--mime] [--formats <директория>]
    Для заданного файла (или файлов) показываются название формата,
    описание формата и тип формата (аудио, видео, архивы и др.).

    С флагом "-m" или "--mime" в вывод включаются MIME-тип и его описание.

    Описания форматов содержатся в специальном JSON-файле, путь
    к которому определяется вашей поставкой библиотеки Amalthea.
    Если библиотека скомпирована компилятором ldc2, путь будет таким:
    /etc/amalthea-ldc2/fileformats.json
    Опция --formats позволяет указать директорию, храняющую JSON-файлы
    с описаниями дополнительных форматов файлов.

    Пример:
        $ fileformat image.jpg -m
        Для файла с именем "image.jpg" будет показана информация о формате
        и его MIME-типе.

fileformat --version
    Отображается версия программы.

fileformat --help
    Показывается эта справка.

© Eugene 'Vindex' Stulin, 2019-2022, 2024
