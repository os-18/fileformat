#!/bin/bash
DC=$1
REQUEST=$2
ARG=$3

LDC2_STATIC_OPTS="-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a,:libz.a"
LDC2_STATIC_OPTS+=" -link-defaultlib-shared=false"
GDC_STATIC_OPTS="-static-libphobos"
DMD_STATIC_OPTS=""

# ldc2 flags
LDC2_DEBUG_OPTIONS="-d-debug --gc"
LDC2_OPTIM_OPTIONS="-O --release --gc"
LDC2_EXTERNAL="-L-l:libgio-2.0.so.0 -L-l:libglib-2.0.so.0"
# dmd flags
DMD_DEBUG_OPTIONS="-debug -g"
DMD_OPTIM_OPTIONS="-O -release"
DMD_EXTERNAL="-L-l:libgio-2.0.so.0 -L-l:libglib-2.0.so.0"
# gdc flags
GDC_DEBUG_OPTIONS="-fdebug"
GDC_OPTIM_OPTIONS="-O2 -frelease"
GDC_EXTERNAL="-lgio-2.0 -lglib-2.0"

AMALTHEA_MAJOR_VERSION=1

if [[ $REQUEST == "phobos_options" && $ARG == static ]]; then
    echo $(eval echo \$${DC^^}_STATIC_OPTS)
elif [[ $REQUEST == "amalthea_options" ]]; then
    if [[ $ARG == static ]]; then
        local flags=$(pkg-config --cflags)
        echo "$flags -L-l:libamalthea-${DC}.a"
    else
        pkg-config --cflags --libs amalthea-${DC}
    fi
elif [[ $REQUEST == "release_options" ]]; then
    echo $(eval echo \$${DC^^}_OPTIM_OPTIONS)
elif [[ $REQUEST == "debug_options" ]]; then
    echo $(eval echo \$${DC^^}_DEBUG_OPTIONS)
elif [[ $REQUEST == "addit_options" ]]; then
    echo $(eval echo \$${DC^^}_EXTERNAL)
fi

if [[ $REQUEST == "doc_dir" ]]; then
    read -ra distr_id_arr <<< "`lsb_release -i`"
    DISTRIBUTION=${distr_id_arr[2]}
    if [[ "${DISTRIBUTION}" != "openSUSE" ]]; then
        echo share/doc
    else
        echo share/doc/packages
    fi
fi
